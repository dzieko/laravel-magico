<?php
/**
 * A base model with a series of CRUD functions (powered by CI's query builder),
 * validation-in-model support, event callbacks and more.
 *
 * @link http://github.com/jamierumbelow/codeigniter-base-model
 * @copyright Copyright (c) 2012, Jamie Rumbelow <http://jamierumbelow.net>
 */
namespace Dzieko\Magico;

class ConfigObj
{
    public function get($id = 0)
    {
        if (isset($this->{$id})) {return $this->{$id};} else {
            return null;
        }
    }
    public function set($id, $value = false)
    {
        if (is_array($id)) {
            foreach ($id as $key => $value) {
                $this->{$key} = $value;
            }
        } elseif (is_object($id)) {
            foreach ($id as $key => $value) {
                $this->{$key} = $value;
            }
        } else {
            $this->{$id} = $value;
        }

        return $this;
    }
}